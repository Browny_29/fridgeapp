<?php
//***
//Select functions
//***

//Fetch all recipes WIP
$app->get('/api/recipe', function() {
    require_once('dbconnect.php');
    $query = "SELECT `id`, `recipe_name` FROM `recipe`";
    $result = $mysqli->query($query);

    while($row = $result->fetch_assoc()){
        $data[] = $row;
    }

    if(isset($data)){
        header('Content-Type: application/json');
        echo json_encode($data);
    }    
});

//Get a recipe by recipe ID
$app->get('/api/recipe/{id}', function($request) {
    require_once('dbconnect.php');

    $recipeId = $request->getAttribute('id');

    $recipe = SelectRecipe($recipeId, $mysqli); 
    $recipe->steps = SelectRecipeSteps($recipeId, $mysqli);
    $recipe->ingredients = SelectRecipeIngredients($recipeId, $mysqli);
    $recipe->tags = SelectRecipeTags($recipeId, $mysqli);
    if($recipe != null && $recipe->steps != null && $recipe->ingredients != null && $recipe->tags != null){
        echo json_encode($recipe);
    } 
    else { echo "false"; }   
});

$app->get('/api/recipes', function($request) {
    require_once('dbconnect.php');

    if (isset($_GET['id'])){
        $idString = $_GET['id'];
        $idArray = explode(',', $idString);
        $recipes = [];
        foreach($idArray as $recipeId){
            $recipe = SelectRecipe($recipeId, $mysqli); 
            $recipe->steps = SelectRecipeSteps($recipeId, $mysqli);
            $recipe->ingredients = SelectRecipeIngredients($recipeId, $mysqli);
            $recipe->tags = SelectRecipeTags($recipeId, $mysqli);
            if($recipe != null && $recipe->steps != null && $recipe->ingredients != null && $recipe->tags != null){
                $recipes[] = $recipe;
            }       
        }
        echo json_encode($recipes);
    }
    else { echo "false"; }   
});

//Select the user, name and description of a recipe by recipe ID
function SelectRecipe($recipeId, $mysqli){
    $query = "SELECT u.id, u.username, r.recipe_name, r.recipe_description, r.number_of_people
                FROM user u 
                LEFT JOIN recipe r
                ON u.id = r.user_id
                WHERE r.id = $recipeId";
    $result = $mysqli->query($query);
    $data = $result->fetch_assoc();

    $recipe = new stdClass();
    if(isset($data)){
        $recipe->recipeID = $recipeId;
        $recipe->userID = $data['id'];
        $recipe->username = $data['username'];
        $recipe->name = $data['recipe_name'];
        $recipe->description = $data['recipe_description'];  
        $recipe->numberOfPeople =   $data['number_of_people'];
        return $recipe;    
    }   
    else { return null; }
};

//Select all the steps of a recipe by recipe ID
function SelectRecipeSteps($recipeId, $mysqli){
    $query = "SELECT s.id, s.step_number, s.step_description
                FROM recipe_step s 
                WHERE s.recipe_id = $recipeId";
    $result = $mysqli->query($query);

    while($row = $result->fetch_assoc()){
        $data[] = $row;
    }

    $steps[] = null;
    if(isset($data)){  
        for ($i=0; $i < sizeof($data); $i++) { 
            $steps[$i] = new stdClass();
            $steps[$i]->id = $data[$i]['id'];
            $steps[$i]->number = $data[$i]['step_number'];
            $steps[$i]->description = $data[$i]['step_description'];
        }
        return $steps;  
    } 
    else { return null; }
};

//Select all the igredients of a recipe by recipe ID
function SelectRecipeIngredients($recipeId, $mysqli){
    $query = "SELECT i.id, i.name, ri.amount, ri.unit, ri.is_optional FROM recipe_ingredient ri
                LEFT JOIN ingredient i 
                ON ri.ingredient_id = i.id
                WHERE ri.recipe_id = $recipeId";
    $result = $mysqli->query($query);

    while($row = $result->fetch_assoc()){
        $data[] = $row;
    }

    $ingredients[] = null;
    if(isset($data)){  
        for ($i=0; $i < sizeof($data); $i++) { 
            $ingredients[$i] = new stdClass();
            $ingredients[$i]->id = $data[$i]['id'];
            $ingredients[$i]->name = $data[$i]['name'];
            $ingredients[$i]->amount = $data[$i]['amount'];
            $ingredients[$i]->unit = $data[$i]['unit'];
            $ingredients[$i]->isOptional = $data[$i]['is_optional'];
        }
        return $ingredients;  
    } 
    else { return null; }
};

function SelectRecipeTags($recipeId, $mysqli){
    $query = "SELECT t.id, t.name FROM recipe_tag rt
                LEFT JOIN tag t 
                ON rt.tag_id = t.id
                WHERE rt.recipe_id = $recipeId";
    $result = $mysqli->query($query);

    while($row = $result->fetch_assoc()){
        $data[] = $row;
    }

    $tags[] = null;
    if(isset($data)){  
        for ($i=0; $i < sizeof($data); $i++) { 
            $tags[$i] = new stdClass();
            $tags[$i]->id = $data[$i]['id'];
            $tags[$i]->name = $data[$i]['name'];

        }
        return $tags;  
    } 
    else { return null; }
};

//***
//Insert functions
//***

//Post data to add a new recipe
$app->post('/api/recipe', function($request) {
    require_once('dbconnect.php');
    session_start();

    if(isset($_SESSION['userid']) && isset($_SESSION['username'])){
        $recipe = $request->getParsedBody();
        $result = InsertRecipeDetails($recipe['Name'], $recipe['Description'], $recipe['NumberOfPeople'], $recipe['UserID'], $mysqli);
        InsertRecipeSteps($result, $recipe['Steps'], $mysqli);
        InsertRecipeIngredients($result, $recipe['Ingredients'], $mysqli);
        InsertRecipeTags($result, $recipe['Tags'], $mysqli);

        echo $result;
    }
    else{
        echo "user not found, no permission"; 
    }
    

});

//Post bulk data to a multiple new recipes
$app->post('/api/recipes', function($request) {
    require_once('dbconnect.php');
    session_start();

    if(isset($_SESSION['userid']) && isset($_SESSION['username'])){
        $recipes = $request->getParsedBody();
        $ids = [];
        foreach ($recipes as $recipe) {
            $recipeId = InsertRecipeDetails($recipe['Name'], $recipe['Description'], $recipe['NumberOfPeople'], $recipe['UserID'], $mysqli);
            InsertRecipeSteps($recipeId, $recipe['Steps'], $mysqli);
            InsertRecipeIngredients($recipeId, $recipe['Ingredients'], $mysqli);
            InsertRecipeTags($recipeId, $recipe['Tags'], $mysqli);
            $ids[] = $recipeId;
        }
        echo json_encode($ids);
    }
    else{
        echo "user not found, no permission";
    }
    
});

//Insert a new recipe into the database 
function InsertRecipeDetails($recipeName, $description, $number_of_people, $user_id, $mysqli) {
    //Insert the recipe information
    date_default_timezone_set('Europe/Amsterdam');
    $date = date('m/d/Y h:i:s a', time());

    $query = "INSERT INTO `recipe` (`user_id`, `recipe_name`, `recipe_description`, `number_of_people`, `added_date`) VALUES (?,?,?,?,?); ";
    $stmt = $mysqli->prepare($query);    
    
    $stmt->bind_param("sssis", $user_id, $recipeName, $description, $number_of_people, $date);

    if ($stmt->execute()) { 
        return $mysqli->insert_id;
    } 
    else {
        return $stmt->error;
    }
    
    
};

//Insert the steps of a recipe into the database
function InsertRecipeSteps($recipe_id, $steps, $mysqli){
    //Insert all the recipe steps
    $query = "INSERT INTO `recipe_step` (`recipe_id`, `step_number`, `step_description`) VALUES (?,?,?)";
    $stmt = $mysqli->prepare($query);
    for($i = 1; $i <= sizeof($steps); $i++){
        $step = (string)$steps[$i-1]['Description'];        
        $stmt->bind_param("iis", $recipe_id, $i, $step);
                    
        $stmt->execute();
    }
};

//Insert the ingredients of a recipe into the database
function InsertRecipeIngredients($recipe_id, $ingredients, $mysqli){
    //Insert all the recipe ingredients
    //Prepare ingredient insert
    $query = "INSERT INTO `ingredient` (`name`) VALUES (?)";
    $stmtNewIngredient = $mysqli->prepare($query);
    //Prepare recipe_ingredient insert
    $query = "INSERT INTO `recipe_ingredient` (`recipe_id`, `ingredient_id`, `amount`, `unit`) VALUES (?,?,?,?)";
    $stmtIngredient = $mysqli->prepare($query);

    for($i = 0; $i < sizeof($ingredients); $i++){
        $name = strtolower($ingredients[$i]['Name']);
        $unit = strtolower($ingredients[$i]['Unit']);
        $amount = (int)$ingredients[$i]['Amount'];

        $query = "SELECT id FROM `ingredient` WHERE name = '$name'";
        $result = $mysqli->query($query);
        $data = $result->fetch_assoc();
        
        $ingredientId = null;
        //If an ingredient does not exsist in the database yet create it
        if(sizeof($data) == 0){       
            $stmtNewIngredient->bind_param("s", $name);      
            $stmtNewIngredient->execute();

            $ingredientId = $mysqli->insert_id;
        }
        else{
            $ingredientId = $data['id'];    
        }
        //Execute the recipe_ingredient insert
        $stmtIngredient->bind_param("iiis", $recipe_id, $ingredientId, $amount, $unit);      
        $stmtIngredient->execute();
    }
};

//Insert the tags of a recipe into the database
function InsertRecipeTags($recipe_id, $tags, $mysqli){
    //Insert all the recipe tags
    //Prepare tag insert
    $query = "INSERT INTO `tag` (`name`) VALUES (?)";
    $stmtNewTag = $mysqli->prepare($query);
    //Prepare tag insert
    $query = "INSERT INTO `recipe_tag` (`recipe_id`, `tag_id`) VALUES (?,?)";
    $stmtTag = $mysqli->prepare($query);

    for($i = 0; $i < sizeof($tags); $i++){
        $name = strtolower($tags[$i]['Name']);

        $query = "SELECT id FROM `tag` WHERE name = '$name'";
        $result = $mysqli->query($query);
        $data = $result->fetch_assoc();
        
        $tagId = null;
        //If a tag does not exsist in the database yet create it
        if(sizeof($data) == 0){       
            $stmtNewTag->bind_param("s", $name);      
            $stmtNewTag->execute();

            $tagId = $mysqli->insert_id;
        }
        else{
            $tagId = $data['id'];    
        }
        //Execute the recipe_tag insert
        $stmtTag->bind_param("ii", $recipe_id, $tagId);      
        $stmtTag->execute();
    }
};

//***
//Update functions
//***

//Update a recipe in the database
$app->put('/api/recipe/{id}', function($request) {
    require_once('dbconnect.php');
    session_start();
    if(isset($_SESSION['userid']) && isset($_SESSION['username'])){
        $recipe = $request->getParsedBody();
        $recipeId = $request->getAttribute('id');

        $result =  UpdateRecipeDetails($recipeId, $recipe['Name'], $recipe['Description'], $recipe['NumberOfPeople'], $mysqli);
        UpdateRecipeSteps($recipeId, $recipe['Steps'], $mysqli);
        UpdateRecipeIngredients($recipeId, $recipe['Ingredients'], $mysqli);
        UpdateRecipeTags($recipeId,  $recipe['Tags'], $mysqli);

        echo json_encode($result);
    }
    else{
        echo "user not found, no permission"; 
    }
    

    
});

//Update a recipe's details in the database 
function UpdateRecipeDetails($recipe_id, $recipeName, $description, $number_of_people, $mysqli) {
    //Update the recipe information

    $query = "UPDATE `recipe` SET `recipe_name`=?, `recipe_description`=?, `number_of_people`=? WHERE `id` = ?; ";
    $stmt = $mysqli->prepare($query);    
    
    $stmt->bind_param("sssi", $recipeName, $description, $number_of_people, $recipe_id);

    $stmt->execute();
};

//Update the steps of a recipe in the database
function UpdateRecipeSteps($recipe_id, $steps, $mysqli){
    $query = "DELETE FROM `recipe_step` WHERE `recipe_id`=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("i", $recipe_id);                    
    $stmt->execute();

    InsertRecipeSteps($recipe_id, $steps, $mysqli); 
};

//Update the ingredients of a recipe in the database
function UpdateRecipeIngredients($recipe_id, $ingredients, $mysqli){
    $query = "DELETE FROM `recipe_ingredient` WHERE `recipe_id`=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("i", $recipe_id);                    
    $stmt->execute();

    InsertRecipeIngredients($recipe_id, $ingredients, $mysqli);
};

//Update the tags of a recipe in the database
function UpdateRecipeTags($recipe_id, $tags, $mysqli){
    $query = "DELETE FROM `recipe_tag` WHERE `recipe_id`=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("i", $recipe_id);                    
    $stmt->execute();

    InsertRecipeTags($recipe_id, $tags, $mysqli);
};

//***
//Remove objects
//***

//Remove a recipe from the database
$app->delete('/api/recipe/{id}', function($request) {
    require_once('dbconnect.php');
    session_start();
    print_r($_SESSION);
    if(isset($_SESSION['userid']) && isset($_SESSION['username'])){
        $recipeId = $request->getAttribute('id');

        $result =  RemoveRecipe($recipeId, $mysqli);

        echo json_encode($result);
    }
    else {
        echo "user not found, no permission"; 
    }
    
});

//Remove the recipe from the database
function RemoveRecipe($recipe_id, $mysqli){
    $query = "DELETE FROM `recipe` WHERE `id`=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("i", $recipe_id);                    
    $stmt->execute();
};
