<?php
$app->post('/api/login', function($request) {
    require_once('dbconnect.php');
    session_start();

    $login = $request->getParsedBody();

    if(isset($login['Username']) && isset($login['Password'])){
        $username = $login['Username'];
        $password = $login['Password'];
        $password = hash("sha256", $password);

        $query = "SELECT `id`, `username`, `name`, `surname`, `date_of_birth`, `address`, `city`, `postalcode`, `phone` FROM `user` WHERE `username`=? && `password`=?";
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param("ss", $username, $password);                    
        $stmt->execute();

        $user = new stdClass();

        $stmt->bind_result($user->ID, $user->Username, $user->Firstname, $user->Surname, $user->DateOfBirth, $user->Address, $user->City, $user->Postalcode, $user->Phonenumber);
        $stmt->fetch();

        if(isset($user)){
            $_SESSION['username'] = $user->Username;
            $_SESSION['userid'] = $user->ID;
            echo json_encode($user);
        } 
        else {
            echo "wrong credentials";
        }
    }
    else{
        echo "credentials given in wrong format";
    }

});