<?php
//Search for a recipe in the database
$app->get('/api/search/recipe', function($request, $app) {
    require_once('dbconnect.php');

    $tagQuery = "SELECT recipe_id FROM recipe_tag WHERE tag_id = ";
    $ingredientQuery = "SELECT recipe_id FROM recipe_ingredient WHERE ingredient_id = ";
    
    $totalScores = [];
    $tagScores = [];
    $ingredientScores = [];
    $recipeScores = [];
    if(isset($_GET['name'])){
        $name = $_GET['name'];
        $result = FindElement($name, "recipe", $mysqli);
        foreach($result as $recipe){
            $recipeScores[$recipe->id] = $recipe->score;
        }
    }
    if (isset($_GET['tags'])){
        $tagString = $_GET['tags'];
        $tagArray = explode(',', $tagString);

        $tagScores = ScoreSearch($tagArray, $tagQuery, $mysqli);
    }
    if (isset($_GET['ingredients'])){
        $ingredientString = $_GET['ingredients'];
        $ingredientArray = explode(',', $ingredientString);

        $ingredientScores = ScoreSearch($ingredientArray, $ingredientQuery, $mysqli);
    }
    $totalScores = $tagScores;
    foreach($ingredientScores as $key => $score){
        if(array_key_exists($key, $totalScores)) $totalScores[$key] += $score;
        else $totalScores[$key] = $score;
    }
    $scoreObj = new stdClass();
    arsort($totalScores);
    arsort($tagScores);
    arsort($ingredientScores);
    arsort($recipeScores);

    $scoreObj->total = $totalScores;
    $scoreObj->tag = $tagScores;
    $scoreObj->ingredient = $ingredientScores;
    $scoreObj->recipe = $recipeScores;

    echo json_encode($scoreObj);
});

//Gives back an array with scored items
function ScoreSearch($elements, $baseQuery, $mysqli){
    $recipeScores = [];
    foreach($elements as $element){
        $query = $baseQuery.$element;
        $result = $mysqli->query($query);
        $data = [];

        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }

        foreach($data as $item){
            if(array_key_exists($item['recipe_id'], $recipeScores)) {
                $recipeScores[$item['recipe_id']]++;
            }
            else {
                $recipeScores[$item['recipe_id']] = 1;
            }
        }            
    }
    return $recipeScores;
}

$app->get('/api/search/tag', function($request, $app) {
    require_once('dbconnect.php');

    if (isset($_GET['tag'])){
        $tagString = $_GET['tag'];

        $tags = FindElement($tagString, "tag", $mysqli);
        echo json_encode($tags);
    }
});

$app->get('/api/search/ingredient', function($request, $app) {
    require_once('dbconnect.php');

    if (isset($_GET['ingredient'])){
        $ingredientString = $_GET['ingredient'];

        $ingredients = FindElement($tagString, "ingredient", $mysqli);
        echo json_encode($ingredients);
    }
});

function FindElement($searchString, $table, $mysqli){
    if($table === "recipe"){
        $query = "SELECT DISTINCT(recipe_name), id FROM $table WHERE recipe_name LIKE \"%$searchString%\"";
    } else $query = "SELECT DISTINCT(name), id FROM $table WHERE name LIKE \"%$searchString%\"";
    
    $result = $mysqli->query($query);
    $data = [];
    $objects = [];
    while($row = $result->fetch_assoc()){
        $data[] = $row;
    }

    foreach($data as $item){
        $object = new stdClass();
        $object->id = $item['id'];
        if($table === "recipe"){
            similar_text(strtoLower($searchString), strtoLower($item['recipe_name']), $percent);
            $object->name = $item['recipe_name'];
        }else{
            similar_text(strtoLower($searchString), strtoLower($item['name']), $percent);
            $object->name = $item['name'];
        }
        
        $object->score = $percent;
        $objects[] = $object;
    }
    return $objects;
};