<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

require_once('../app/api/recipe.php');
require_once('../app/api/login.php');
require_once('../app/api/search.php');
require_once('../app/api/user.php');


$app->run();